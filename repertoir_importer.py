#!/usr/bin/env python

"""Import choral works from csv-table. Run
import_all()
from "manage.py shell".
"""

import sys
import os
import csv
from django.core.exceptions import ObjectDoesNotExist

djangoprojecthome = os.path.abspath("./djangochoir")
csv_filepathname = os.path.abspath("./Repertoar.csv")


sys.path.append(djangoprojecthome)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'


import django

django.setup()

from choir.models import Person, Composition, Category, AuthorOf, Event


def get_names():
    with open("nevek.txt") as f:
        names = f.readlines()
    name_list = []
    for row in names:
        name_tuple = get_first_and_last_name(row)
        name_list.append(name_tuple)
    return name_list


def get_first_and_last_name(row):
    ordering_mode, name = row.split(maxsplit=1)
    name = name.strip()
    splitted_name = name.split()
    suffix = ""
    EASTERN, WESTERN = 1, 0
    name_order = EASTERN
    if ordering_mode == "u":
        name_order = WESTERN
        family_name = splitted_name[-1]
        given_name = " ".join(splitted_name[:-1])
    elif ordering_mode == "1":
        family_name = splitted_name[0]
        given_name = " ".join(splitted_name[1:])
    elif ordering_mode == "2":
        suffix = splitted_name[0]
        family_name = splitted_name[1]
        given_name = " ".join(splitted_name[2:])
    return suffix, given_name, family_name, name_order


def import_names():
    for suffix, given_name, family_name, name_order in get_names():
        person = Person(
            suffix=suffix,
            given_name=given_name,
            family_name=family_name,
            name_order=name_order,
        )
        person.save()


def create_category(category_name):
    Category.objects.create(category_name=category_name)


def create_composition(title, category_name):
    composition = Composition.objects.create(title=title)
    category = Category.objects.get(category_name=category_name)
    composition.categories.add(category)


def import_all():
    print("importing names...")
    import_names()
    print("importing categories ...")
    category_name_set = get_fields_from_csv(-1)
    for c in category_name_set:
        create_category(c[0])
    print("importing compositions ...")
    compositions = get_fields_from_csv([1, -1])
    for title, category in compositions:
        create_composition(title, category)
    print("importing authors of ...")
    import_authors_of()
    print("set the type of text authors ...")
    set_author_type(text_authors, "T")
    print("correct the name of G. Dénes ...")
    correct_gdenes()
    print("create events ...")
    create_events(event_dict_list)


def add_composer_to_composition(title, name):
    title = title.strip()
    try:
        composition = Composition.objects.get(title=title)
    except ObjectDoesNotExist:
        print("|{}| nem létezik".format(title))
    persons = find_person_with_name(name)
    if not persons:
        print("Nincs találat erre: {name}: {title}".format(**locals()))
        return
    person = persons[0]
    AuthorOf.objects.create(
        person=person,
        composition=composition
    )


def recover_alleluias():
    for alleluia in Composition.objects.filter(title__startswith="Alleluia"):
        print(alleluia.title)
    print("folytatni")


def import_authors_of():
    pairs = get_name_composition_pairs_from_csv()
    for name, title in pairs:
        add_composer_to_composition(title, name)


def find_person_with_name(name):
    return [
        p
        for p in Person.objects.all()
        if p.get_full_name() == name
    ]


# Data from csv file


def csv_reader_creator(csv_filepathname=csv_filepathname):
    return csv.reader(open(csv_filepathname), delimiter=',')


def get_names_from_csv():
    szerzok = []
    for row in csv_reader_creator():
        szerzoi = row[0].split("/")
        szerzok.extend(szerzoi)
    print(len(szerzok))
    szerzok = set([szerzo.strip() for szerzo in szerzok])
    print(len(szerzok))
    return set(szerzok)


def get_fields_from_csv(indexes):
    csv_reader = csv_reader_creator()
    if isinstance(indexes, int):
        indexes = [indexes]
    values = []
    for row in csv_reader:
        if row[0] != 'Szerző':
            values.append(tuple([row[i].strip() for i in indexes]))
    return set(values)

# names = get_names_from_csv(csv_reader)
# print(names)


def get_name_composition_pairs_from_csv():
    csv_reader = csv_reader_creator()
    name_composition_pairs = []
    for row in csv_reader:
        if row[0] != 'Szerző':
            authors = row[0].split("/")
            if not authors:
                continue
            title = row[1]
            name_composition_pairs.extend([(name.strip(), title) for name in authors if name])
    return name_composition_pairs

text_authors = (
    dict(family_name="József", given_name="Attila"),
    dict(family_name="Weöres", given_name="Sándor"),
)


def set_author_type(author_dictlist, type_="T"):
    for dictionary in author_dictlist:
        person = Person.objects.get(**dictionary)
        print(person)
        for authorof in person.authorof_set.all():
            authorof.type = type_


def correct_gdenes():
    gdenes = Person.objects.get(family_name="G.")
    gdenes.family_name = "G. Dénes"
    gdenes.given_name = "György"
    gdenes.save()


# from django.utils import timezone
import datetime

event_dict_list = [
    dict(name="20 év - 20 dal", date=datetime.date(2017, 6, 9), time=datetime.time(18)),
    dict(name="Karácsonyi koncert", date=datetime.date(2016, 12, 14), time=datetime.time(19)),
]


def create_events(event_dict_list):
    for event_dict in event_dict_list:
        Event.objects.create(**event_dict)
