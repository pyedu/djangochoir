from datetime import date

from django.db import models

from choir.models import Member


class Poem(models.Model):
    class Meta:
        verbose_name = "vers"
        verbose_name_plural = "versek"

    poem = models.TextField("vers", max_length=5000, blank=True, help_text="""A vers sima szövegként""")
    author = models.ForeignKey(Member, related_name='poem_from', on_delete=models.CASCADE, verbose_name="szerző",
                               blank=True, null=True)
    to = models.ForeignKey(Member, related_name='poem_to', on_delete=models.CASCADE, verbose_name="kinek",
                           blank=True, null=True)
    year = models.IntegerField("év", default=date.today().year)
    file = models.FileField("csatolmány", upload_to="christmas/", blank=True, null=True, default=None)

    def __repr__(self):
        return f"{self.year} {self.author} - {self.to}"

    def __str__(self):
        return self.__repr__()
