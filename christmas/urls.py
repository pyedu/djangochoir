from django.urls import re_path
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    re_path(r'^$', RedirectView.as_view(pattern_name='show_all')),
    re_path(r'^all/$', views.show_all, name='show_all'),
    re_path(r'^year/(\d+)$', views.show_year, name='show_year'),
    re_path(r'^person/to/(\d+)$', views.show_to_person, name='show_to_person'),
    re_path(r'^person/from/(\d+)$', views.show_author, name='show_author'),
]
