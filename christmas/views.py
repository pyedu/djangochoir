from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from christmas import models


@login_required(login_url="/admin/login/")
def show_all(request):
    poems = models.Poem.objects.all().order_by('-year')
    return render(request, 'christmas/poems.html', context={"poems": poems})


@login_required(login_url="/admin/login/")
def show_year(request, id):
    poems = models.Poem.objects.filter(year=int(id)).order_by('to')
    return render(request, 'christmas/poems.html', context={"poems": poems})


@login_required(login_url="/admin/login/")
def show_to_person(request, id):
    poems = models.Poem.objects.filter(to=int(id)).order_by('-year')
    return render(request, 'christmas/poems.html', context={"poems": poems})


@login_required(login_url="/admin/login/")
def show_author(request, id):
    poems = models.Poem.objects.filter(author=int(id)).order_by('-year')
    return render(request, 'christmas/poems.html', context={"poems": poems})
