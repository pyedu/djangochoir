from django.contrib import admin
from .models import (AuthorOf, Piece, Person, Member, Category,
        Sheet, Event, Venue, Settlement,
        PracticeRecording, ConcertRecording)
from markdownx.admin import MarkdownxModelAdmin


class PieceAdmin(MarkdownxModelAdmin):
    search_fields = ('title',)
admin.site.register(Piece, PieceAdmin)

class PersonAdmin(MarkdownxModelAdmin):
    search_fields = ('family_name', 'given_name')
admin.site.register(Person, PersonAdmin)

admin.site.register(Member)


class AuthorOfAdmin(admin.ModelAdmin):
    search_fields = ('piece__title', 'person__family_name')
admin.site.register(AuthorOf, AuthorOfAdmin)

admin.site.register(Category)

class SheetAdmin(MarkdownxModelAdmin):
    search_fields = ('piece__title', 'piece__authors__family_name')
admin.site.register(Sheet, SheetAdmin)


class ConcertRecordingAdmin(MarkdownxModelAdmin):
    search_fields = ('piece__title', 'piece__authors__family_name')
    filter_horizontal = ('pieces',)
admin.site.register(ConcertRecording, ConcertRecordingAdmin)


class PracticeRecordingAdmin(MarkdownxModelAdmin):
    search_fields = ('piece__title', 'piece__authors__family_name')
admin.site.register(PracticeRecording, PracticeRecordingAdmin)

admin.site.register(Venue)
admin.site.register(Settlement)


class EventAdmin(MarkdownxModelAdmin):
    filter_horizontal = ('piece',)
admin.site.register(Event, EventAdmin)
