# Generated by Django 2.0.2 on 2020-05-02 06:14

from django.db import migrations, models
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
        ('choir', '0009_auto_20200501_1513'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormattedText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Az oldal címe, illetve a speciális tartalom azonosítója dupla alulvonással keretezve az utóbbi esetben e.g. __choir_intro__', max_length=33, verbose_name='cím')),
                ('text', markdownx.models.MarkdownxField(max_length=5000, verbose_name='markdown szöveg')),
            ],
        ),
    ]
