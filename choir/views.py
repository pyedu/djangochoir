from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from choir import models
from django.utils import timezone
from django.urls import reverse
from django.forms import modelform_factory

def user_logout(request):
    logout(request)
    return redirect('events')


def search(request):
    pieces = []
    list_title = "Keress a kórus repertoárjában!"
    pattern = None
    if request.POST:
        pattern = request.POST.get("pattern")
        pieces = models.Piece.search(pattern)
        list_title = 'A "{}" mintára illeszkedő művek'.format(pattern)
    context = {
        'pieces': pieces,
        'list_title': list_title,
        'pattern': pattern
    }
    return render(request, "choir/search.html", context)


def authors(request):
    authors = models.Person.objects.all().order_by("family_name")
    context = {
        'authors': authors
    }
    return render(request, "choir/authors.html", context)


def categories(request):
    categories = models.Category.objects.all().order_by("category_name")
    context = {
        'categories': categories,
        'piece_number': models.Piece.objects.count(),
    }
    return render(request, "choir/categories.html", context)


def events(request):
    events = models.Event.objects.all().order_by("-date")
    context = {
        'events': events
    }
    template_name = {
        True: "choir/events.html",
        False: "choir/events_public.html",
    }[request.user.is_authenticated]
    return render(request, template_name, context)


def wishlist(request):
    whishlist_event_id = 40  # This is a temporary heck
    event = get_object_or_404(models.Event, id=whishlist_event_id)
    context = {
        'event': event
    }
    template_name = "choir/wishlist.html",
    return render(request, template_name, context)


def event_details(request, id):
    event = get_object_or_404(models.Event, id=id)
    context = {
        'event': event
    }
    template_name = "choir/event_details.html",
    return render(request, template_name, context)


def piece_detail(request, id):
    piece = models.Piece.objects.get(id=id)
    context = dict(piece=piece)
    return render(request, "choir/piece_detail.html", context)


def choir_details(request):
    leaders = models.Member.objects.filter(is_leader=True).order_by("-name")
    members = models.Member.objects.filter(is_leader=False, is_active=True).order_by('name')
    choir_intro = models.FormattedText.objects.get(title="__choir_intro__").text
    all_members = models.Member.objects.all()
    earlier_members = models.Member.objects.filter(is_active=False).order_by('name')

    context = dict(leaders=leaders, members=members, earlier_members=earlier_members,
                   all_members=all_members,
                   months=models.MONTH_NAMES[1:],
                   choir_intro=choir_intro,
                   )
    return render(request, "choir/choir_details.html", context)


@login_required(login_url="/admin/login/")
def upload_sheet(request, piece_id):

    piece = models.Piece.objects.get(id=piece_id)
    Sheet_Form = modelform_factory(models.Sheet, fields=("sheet", "comment"))
    if request.method == "POST":
        form = Sheet_Form(request.POST, request.FILES)
        if form.is_valid():
            sheet = models.Sheet(
                sheet=request.FILES['sheet'],
                piece=piece,
                comment=form.cleaned_data['comment'],
            )
            sheet.piece = piece
            sheet.save()
            return HttpResponseRedirect(reverse('piece_detail', args=[piece.id]))
    else:
        form = Sheet_Form()

    context = dict(piece=piece, form=form)
    return render(request, "choir/upload_sheet.html", context)


@login_required(login_url="/admin/login/")
def upload_practice_recording(request, piece_id):

    piece = models.Piece.objects.get(id=piece_id)
    PracticeRecording_Form = modelform_factory(
        models.PracticeRecording,
        fields=("audio_file", "part", "comment", "own_record"),
        #help_texts={
        #    "own_record": "...."},
    )
    if request.method == "POST":
        form = PracticeRecording_Form(request.POST, request.FILES)
        if form.is_valid():
            record = form.save(commit=False)
            record.piece=piece
            record.save()
            return HttpResponseRedirect(reverse('piece_detail', args=[piece.id]))
    else:
        form = PracticeRecording_Form()

    context = dict(piece=piece, form=form)
    return render(request, "choir/upload_record.html", context)

""" depreciated
@login_required(login_url="/admin/login/")
def sheets(request):
    sheets = models.Sheet.objects.all().order_by("piece")
    context = {
        'sheets': sheets
    }
    return render(request, "choir/sheets.html", context)


@login_required(login_url="/admin/login/")
def upload_main(request, id):
    \"""Easy upload from mobile phone\"""
    piece = models.Piece.objects.get(id=id)
    context = dict(piece=piece)
    return render(request, "choir/upload_main.html", context)


from django.forms import ModelForm
from .models import PracticeRecording
class PracticeRecordingForm(ModelForm):
    class Meta:
        model = PracticeRecording
        fields = "audio_file part comment".split()


@login_required(login_url="/admin/login/")
def upload_voice_type(request, id):
    piece = models.Piece.objects.get(id=id)
    context = dict(
            piece=piece,
            form=PracticeRecordingForm(request.POST, request.FILES),
            )
    return render(request, "choir/upload_voice_type.html", context)

@login_required(login_url="/admin/login/")
def upload_voice_type_result(request, id):
    piece = get_object_or_404(models.Piece, pk=id)
    return HttpResponseRedirect(reverse("upload_voice_type", args=(piece.id,)))

@login_required(login_url="/admin/login/")
def upload_concert(request, id):
    piece = models.Piece.objects.get(id=id)
    context = dict(piece=piece)
    return render(request, "choir/upload_concert.html", context)
"""
