from django.apps import AppConfig


class ChoirConfig(AppConfig):
    name = 'choir'
