from django.urls import re_path
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    re_path(r'^$', RedirectView.as_view(pattern_name='events')),
    re_path(r'^logout/$', views.user_logout, name='logout'),
    # re_path(r'^sheets/$', views.sheets, name='sheets'),
    re_path(r'^events/$', views.events, name='events'),
    re_path(r'^wishlist/$', views.wishlist, name='wishlist'),
    re_path(r'^events/(\d+)$', views.event_details, name='event_details'),
    re_path(r'^choir/$', views.choir_details, name='choir_details'),

    re_path(r'^pieces/$', views.search, name='search'),
    re_path(r'^pieces/authors/$', views.authors, name='authors'),
    re_path(r'^pieces/categories/$', views.categories, name='categories'),
    re_path(r'^pieces/(\d+)$', views.piece_detail, name='piece_detail'),

# Depreciated
    re_path(r'^repertoire/search$', views.search, name='repertoire_search'),
    re_path(r'^repertoire/authors/$', views.authors, name='repertoire_authors'),
    re_path(r'^repertoire/categories/$', views.categories, name='repertoire_categories'),
    re_path(r'^repertoire/(\d+)$', views.piece_detail, name='repertoire_piece_detail'),

#     re_path(r'^pieces/(\d+)/upload/main$', views.upload_main, name='upload_main'),
#     re_path(r'^pieces/(\d+)/upload/voice_type/result$', views.upload_voice_type_result, name='upload_voice_type_result'),
#     re_path(r'^pieces/(\d+)/upload/concert$', views.upload_sheet, name='upload_concert'),
     re_path(r'^pieces/(\d+)/upload/practicing_record$', views.upload_practice_recording, name='upload_practice_recording'),
     re_path(r'^pieces/(\d+)/upload/sheet$', views.upload_sheet, name='upload_sheet'),
]
