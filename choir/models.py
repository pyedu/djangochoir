from django.db import models
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from datetime import datetime
import unicodedata


def remove_accent_lower(text: str) -> str:
    return unicodedata.normalize('NFKD', text).lower().replace("ø", "o").encode('ASCII', 'ignore').decode('ASCII')


MONTH_NAMES = [
    '', 'január', 'február', 'március', 'április', 'május', 'június',
    'július', 'augusztus', 'szeptember', 'október', 'november', 'december'
]


# import calendar
# MONTH_NAMES = calendar.month_name


class Person(models.Model):
    class Meta:
        verbose_name = "személy"
        verbose_name_plural = "személyek"
        ordering = ["family_name"]

    suffix = models.CharField("előtag", max_length=70, blank=True, default="")
    family_name = models.CharField("családnév", max_length=70)
    given_name = models.CharField("keresztnév", max_length=70)
    NAME_ORDERS = (
        (0, "Western"),
        (1, "Eastern"),
    )
    name_order = models.IntegerField(
        choices=NAME_ORDERS,
    )

    def get_full_name(self):
        if self.name_order == 0:
            first = self.given_name
            second = self.family_name
        elif self.name_order == 1:
            first = self.family_name
            second = self.given_name
        name = f"{first} {second}"
        if self.suffix:
            name = self.suffix + " " + name
        return name

    def get_listed_name(self, bold_family=False):
        if bold_family:
            format_string = "<b>{0.family_name}</b>, {0.given_name}"
        else:
            format_string = "{0.family_name}, {0.given_name}"

        if self.suffix:
            format_string = "{0.suffix} " + format_string
        return format_string.format(self)

    def get_bold_listed_name(self):
        return self.get_listed_name(bold_family=True)

    def __str__(self):
        return self.get_listed_name()


class Member(models.Model):
    DAY_CHOICES = [(i, i) for i in range(1, 32)]
    MONTH_CHOICES = [(i, MONTH_NAMES[i]) for i in range(1, 13)]
    PARTS = (
        ("S", "szoprán"),
        ("A", "alt"),
        ("T", "tenor"),
        ("B", "basszus"),
    )
    name = models.CharField("Név", max_length=200)

    email = models.EmailField("Email", blank=True, null=True)
    phone = models.CharField("Telefonszám", max_length=40, blank=True, null=True)

    birth_month = models.IntegerField("Születési hónap", choices=MONTH_CHOICES)
    birth_day = models.IntegerField("Születési nap", choices=DAY_CHOICES)

    name_month = models.IntegerField("Névnap hónapja", choices=MONTH_CHOICES)
    name_day = models.IntegerField("Névnap napja", choices=DAY_CHOICES)

    part = models.CharField("Szólam", max_length=1, choices=PARTS)

    public_notes = models.TextField("Szöveg a honlapra", max_length=250, blank=True)

    address = models.CharField("Cím", max_length=200)

    image = models.ImageField("Arckép honlapra", blank=True, null=True)

    is_leader = models.BooleanField("Karvezető?", default=False)

    is_active = models.BooleanField(
        "Aktív",
        default=True,
        help_text="Az aktív tagok jelennek meg a bemutatkozóoldalon."
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "kórustag"
        verbose_name_plural = "kórustagok"


class AuthorOf(models.Model):
    class Meta:
        verbose_name = "mű szerzője"
        verbose_name_plural = "művek szerzői"

    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    piece = models.ForeignKey("Piece", on_delete=models.CASCADE)
    AUTHOR_TYPES = (
        ("C", "zene"),
        ("c", "zene és szöveg"),
        ("T", "szöveg"),
        ("A", "átdolgozta"),
    )
    type = models.CharField(
        max_length=1,
        choices=AUTHOR_TYPES,
        default="C"
    )

    def __str__(self):
        return f"{self.person.get_full_name()} ({self.get_type_display()}): {self.piece.title}"


class Piece(models.Model):
    class Meta:
        verbose_name = "mű"
        verbose_name_plural = "művek"
        ordering = ['title']

    title = models.CharField("a mű címe", max_length=100)
    authors = models.ManyToManyField(
        Person,
        through='AuthorOf',
        verbose_name="szerző",
    )
    categories = models.ManyToManyField(
        "Category",
        verbose_name="kategória",
    )
    # is_whish_list = models.BooleanField(
    #     "Ez a darab kívánságlistás? Ha már szerepelt koncerten, akkor hamis.",
    #     default=False)
    comment = MarkdownxField(
        "megjegyzések", max_length=5000, blank=True,
        help_text=(
            "Bármilyen szöveg írható ide, ami segítséget ad a műhöz: miért van két kottaváltozat, "
            "felkonferáló szövegek, mások felvételeire hivatkozás. A markdown jelölőnyelv használható.")
    )

    @property
    def markdown_formatted_comment(self):
        return markdownify(self.comment)

    def __str__(self):
        authors = self.authors.all()
        author_names = "(" + ", ".join(a.family_name for a in authors) + ")" if authors else ""
        return f"{self.title} {author_names}"

    def attachements(self):
        attachements = []
        glypicon_template = '<span title="{0}" class="glyphicon glyphicon-{1}"></span>'
        if self.sheet_set.all():
            attachements.append(f"{self.sheet_set.count()} {glypicon_template.format('kotta', 'book')}")
        if self.practicerecording_set.all():
            attachements.append(f"{self.practicerecording_set.count()} {glypicon_template.format('gyakorló felvétel', 'music')}")
        if self.concertrecording_set.all():
            attachements.append(f"{self.concertrecording_set.count()} {glypicon_template.format('koncertfelvétel', 'cd')}")
        if self.comment:
            attachements.append(glypicon_template.format("megjegyzés", "comment"))
        if attachements:
            return f"({', '.join(attachements)})"
        return ""

    @staticmethod
    def search(pattern):
        """Find pieces matching a pattern."""
        words = pattern.split()

        pieces = []
        for c in Piece.objects.all():
            if all(c.is_word_in_piece_or_author(word)
                   for word in words):
                pieces.append(c)
        return pieces

    def is_word_in_piece_or_author(self, word):
        """Case and accent insensitive."""
        word_no_accent = remove_accent_lower(word)
        if word_no_accent in remove_accent_lower(self.title):
            return True
        authors = self.authors.all()
        for author in authors:
            if word_no_accent in remove_accent_lower(author.family_name) or word_no_accent in remove_accent_lower(author.given_name):
                return True
        return False

    def get_own_records(self):
        own_records = self.practicerecording_set.filter(own_record=True)
        return own_records

    def get_foreign_records(self):
        return self.practicerecording_set.filter(own_record=False)


class Category(models.Model):
    class Meta:
        verbose_name = "kategória"
        verbose_name_plural = "kategóriák"
        ordering = ['category_name']

    def __str__(self):
        return self.category_name

    category_name = models.CharField('kategória', max_length=60)
    parent = models.ManyToManyField(
        "self",
        verbose_name="szülőkategória",
        blank=True,
    )


class Sheet(models.Model):
    class Meta:
        verbose_name = "kotta"
        verbose_name_plural = "kották"

    sheet = models.FileField("fájl")
    piece = models.ForeignKey(
        Piece,
        on_delete=models.CASCADE,
        verbose_name="mű",
    )
    comment = MarkdownxField(
        "megjegyzések", max_length=5000, blank=True,
        help_text=(
            "Bármilyen szöveg írható ide, ami segítséget ad a kotta használatához. "
            "A markdown jelölőnyelv használható."
        )
    )

    @property
    def markdown_formatted_comment(self):
        return markdownify(self.comment)

    def __str__(self):
        return self.piece.title


class Event(models.Model):
    class Meta:
        verbose_name = "esemény"
        verbose_name_plural = "események"

    name = models.CharField(
        "esemény neve", max_length=200,
        help_text="Ilyen néven jelenik meg az eseménynaptárban.")
    date = models.DateField(
        'dátuma',
        help_text="Több napos eseménynél a kezdőnap dátuma.")
    is_date_estimated = models.BooleanField("A dátum bizonytalan", default=False)
    time = models.TimeField(
        'időpontja',
        blank=True, null=True,
        help_text="A kezdetének az időpontja.")
    piece = models.ManyToManyField(
        Piece,
        verbose_name="mű",
        blank=True,
    )
    is_public = models.BooleanField(
        "Publikus esemény", default=True,
        help_text="A koncertek nem publikusak, amíg nincs meghívó.")
    venue = models.ForeignKey("Venue", null=True, on_delete=models.CASCADE)
    image = models.ImageField("Meghívó képként", blank=True, null=True)
    comment = MarkdownxField(
        "megjegyzések", max_length=5000, blank=True,
        help_text=(
            "Bármilyen szöveg írható ide, amit az eseménnyel kapcsolatban hasznos tudni a kórus tagjai számára."
            " A markdown jelölőnyelv használható."
        )
    )
    recordings_directory = models.CharField(
        "hangfájlok mappája",
        max_length=40,
        blank=True,
        help_text="A könyvtár (mappa) neve, ahol a koncerten készült felvételek vannak."
    )

    @property
    def markdown_formatted_comment(self):
        return markdownify(self.comment)

    def __str__(self):
        return f"{self.name}, {self.date}"

    @property
    def is_future_event(self):
        return self.date >= datetime.now().date()


class Settlement(models.Model):
    class Meta:
        verbose_name = "település"
        verbose_name_plural = "települések"

    counties = list(enumerate((
        "Budapest", "Pest", "Fejér", "Komárom-Esztergom",
        "Veszprém", "Győr-Moson-Sopron", "Vas", "Zala",
        "Baranya", "Somogy", "Tolna", "Borsod-Abaúj-Zemplén",
        "Heves", "Nógrád", "Hajdú-Bihar",
        "Jász-Nagykun-Szolnok", "Szabolcs-Szatmár-Bereg",
        "Bács-Kiskun", "Békés", "Csongrád",
        "Ausztria", "Románia", "Szlovákia",
        "Ukrajna", "Horvátország", "Szerbia", "Szlovénia",
        "nem szomszédos ország",
    )))
    name = models.CharField("település neve", max_length=100, unique=True)
    county = models.IntegerField(
        "megye (vagy ország)", choices=counties, blank=True, null=True,
        help_text=("Magyar településeknél válassza ki a megyét, "
                   "szomszédos országoknál az ország nevét, "
                   "más esetekben a \"nem szomszédos ország\" lehetőséget. "
                   "<br>\nAz utóbbi esetben a település neve után "
                   "írhatja az ország nevét vesszővel elválasztva.")
    )
    longitude = models.FloatField("földrajzi hosszúság", blank=True, null=True)
    latitude = models.FloatField("földrajzi szélesség", blank=True, null=True)

    def __str__(self):
        return self.name


class Venue(models.Model):
    class Meta:
        verbose_name = "helyszín"
        verbose_name_plural = "helyszínek"

    name = models.CharField(
        "Név", max_length=100, unique=True,
        help_text=("Ez jelenik meg a helyszín oldalán. "
                   "Az előadásoknál megjelenő névváltozatok "
                   "(névváltozási, címváltozás) más táblában szerepelnek.")
    )
    description = models.TextField(
        "leírás",
        help_text=("Ide írhat egy összefoglalást a helyszínről. "
                   "(Megközelítés, honlap, kapcsolati információk...)"),
        blank=True)
    telepules = models.ForeignKey(Settlement, verbose_name="település", on_delete=models.CASCADE)
    zip_code = models.IntegerField("irányítószám", blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    email = models.EmailField(
        'e-mail címe', max_length=100, blank=True,
        help_text="Az aktuális e-mailcíme. A helyszín honlapján jelenik meg."
    )

    def __str__(self):
        return self.name


class PracticeRecording(models.Model):
    class Meta:
        verbose_name = "szólamfelvétel"
        verbose_name_plural = "szólamfelvételek"

    PARTS = (
        ("ALL", "minden szólam"),
        ("FEM", "női szólamok"),
        ("MAL", "férfi szólamok"),
        ("S", "Szoprán"),
        ("S1", "Szoprán 1"),
        ("S2", "Szoprán 2"),
        ("M", "Mezzo"),
        ("A", "Alt"),
        ("A1", "Alt 1"),
        ("A2", "Alt 2"),
        ("T", "Tenor"),
        ("T1", "Tenor 1"),
        ("T2", "Tenor 2"),
        ("BAR", "Bariton"),
        ("B", "Basszus"),
        ("B1", "Basszus 1"),
        ("B2", "Basszus 2"),
    )
    audio_file = models.FileField("hangfájl")
    piece = models.ForeignKey(
        Piece,
        on_delete=models.CASCADE,
        verbose_name="mű",
    )
    part = models.CharField(
        "szólam",
        max_length=4,
        choices=PARTS,
    )
    comment = MarkdownxField(
        "megjegyzések", max_length=5000, blank=True,
        help_text=(
            "Bármilyen szöveg írható ide a fájllal kapcsolatban, ami segítséget ad a darab tanuláshoz."
            " A markdown jelölőnyelv használható."
        )
    )

    @property
    def markdown_formatted_comment(self):
        return markdownify(self.comment)

    pub_date = models.DateField("feltöltés dátuma", auto_now=True)
    own_record = models.BooleanField(
        "saját felvétel",
        default=True,
        help_text="Az énekkar saját felvétele-e?"
    )

    def __str__(self):
        return f"{self.piece.title}, {dict(self.PARTS)[self.part]}"


class ConcertRecording(models.Model):
    class Meta:
        verbose_name = "koncert- vagy CD-felvétel"
        verbose_name_plural = "koncert- és CD-felvételek"

    audio_file = models.FileField("hangfájl", upload_to="concert_recordings/")
    pieces = models.ManyToManyField(
        Piece,
        # on_delete=models.CASCADE,
        verbose_name="mű",
        blank=True,
    )

    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        verbose_name="esemény",
    )

    comment = MarkdownxField(
        "megjegyzések", max_length=5000, blank=True,
        help_text="""Bármilyen szöveg írható ide a fájllal kapcsolatban. A markdown jelölőnyelv használható."""
    )

    @property
    def markdown_formatted_comment(self):
        return markdownify(self.comment)

    pub_date = models.DateField("feltöltés dátuma", auto_now=True)

    def piece_titles(self):
        titles = [piece.title for piece in self.pieces.all()]
        return ", ".join(titles)

    def __str__(self):
        return f"{self.piece_titles()}, {self.event}"


class FormattedText(models.Model):
    title = models.CharField("cím", max_length=33, help_text="Az oldal címe, illetve a speciális tartalom azonosítója dupla alulvonással keretezve az utóbbi esetben e.g. __choir_intro__")
    text = MarkdownxField("markdown szöveg", max_length=5000)
