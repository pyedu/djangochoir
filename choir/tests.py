from django.test import TestCase
from .models import Piece, Person, AuthorOf


def is_piece_in_iterable(author_start, title_start, pieces):
    for piece in pieces:
        if piece.title.startswith(title_start):
            for author in piece.authors.all():
                if author.family_name.startswith(author_start):
                    return True
    return False


class PieceSearchTest(TestCase):
    known_values = (
        ("rutter", "Rutter", "Esurientes"),
        ("esuri rutter", "Rutter", "Esurientes"),
        ("miguel", "Matamoros", "Juramento"),
        ("sur avignon", "Streichard", "Sur Le Pont"),
    )

    @classmethod
    def setUpTestData(cls):
        c = Piece(title="The Little Drummer Boy")
        c.save()
        c1 = Piece(title="Esurientes, Magnificat IX.")
        c1.save()
        c2 = Piece(title="Juramento")
        c2.save()
        c3 = Piece(title="Sur Le Pont D'Avignon")
        c3.save()

        p = Person(family_name="Davis", given_name="Katherine", name_order=0)
        p.save()
        p1 = Person(family_name="Onorati", given_name="Henry", name_order=0)
        p1.save()
        p2 = Person(family_name="Harry", given_name="Simeone", name_order=0)
        p2.save()
        p3 = Person(family_name="Rutter", given_name="John", name_order=0)
        p3.save()
        p4 = Person(family_name="Matamoros", given_name="Miguel", name_order=0)
        p4.save()
        p5 = Person(family_name="Streichard", given_name="Antonius", name_order=0)
        p5.save()

        authors = {
            c: [p, p1, p2],
            c1: [p3],
            c2: [p4],
            c3: [p5],
        }

        for piece in authors:
            for person in authors[piece]:
                ao = AuthorOf(person=person, piece=piece)
                ao.save()

    def test_piece_search(self):
        for pattern, author_start, title_start in self.known_values:
            results = Piece.search(pattern)
            self.assertTrue(is_piece_in_iterable(
                author_start,
                title_start,
                results)
            )

    def test_is_word_in_piece_or_author(self):
        c = Piece.objects.get(title__startswith="The")
        good_values = ["drummer", "DRUMMER", "onora", "Sime"]
        for word in good_values:
            self.assertTrue(c.is_word_in_piece_or_author(word))
        bad_values = ["Matamoros", "dummer", "debussy"]
        for word in bad_values:
            self.assertFalse(c.is_word_in_piece_or_author(word))
