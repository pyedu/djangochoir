I saved the copy of the dir into the fedora laptop Downloads at
2023. aug. 26., szombat
-rw-r--r--. 1 ha ha    2028183 Aug 26 22:44  staccato.pyedu.hu.tar.gz
-rw-r--r--. 1 ha ha   44568471 Aug 26 22:45  courses.pyedu.hu.tar.gz



## Start for development

### Download source

### Create and activate virtual environment

    python3 -m venv virtualenv
    . virtualenv/bin/activate
    pip install -r requirements.txt

### Migrate database and start the server

(Type python before every manage.py if it doesn't work.)

    manage.py migrate
    manage.py runserver

## Migrate from sqlite3 to PostgreSQL

This worked for me ([source](https://stackoverflow.com/questions/7002194/how-to-copy-database-in-use-to-other-database-in-django)):

First, execute 

    manage.py dumpdata > out.json

then change your DB config, migrate (or syncdb) and finally

    echo "delete from auth_permission; delete from django_content_type;" | python manage.py dbshell

Then load the JSON file:

    manage.py loaddata out.json

(as of 2013 `django_contenttype` is replaced with `django_content_type`)

## Useful URLs

https://coderwall.com/p/mvsoyg/django-dumpdata-and-loaddata
https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html
