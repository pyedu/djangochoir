"""djangochoir URL Configuration
"""
from django.urls import re_path, include
from django.contrib import admin

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^markdownx/', include('markdownx.urls')),
    re_path(r'^xmas/', include('christmas.urls')),
    re_path(r'^', include('choir.urls')),
]
