# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': "staccato",
        'USER': "ha",
        'PASSWORD': "ha",
    }
}


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0clm-t39d2a8hecgla*2uj_8e&3p5#&sanl%rr%8_7i3c=qhz#'

MEDIA_ROOT = '/home/ha/dev.staccato.pyedu.hu/choir_media/'
MEDIA_URL = 'http://localhost/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


